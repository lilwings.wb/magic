import { PopupExtend } from "../qzilla/popupExtend/popupExtend";

export class Popup extends PopupExtend {

    constructor() {
        super();
    }

    onClick(event: MouseEvent) {
        // Soon
    }

    listener() {
        document.addEventListener('click', (event) => { this.onClick(event) });
    }

}